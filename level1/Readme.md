# Plan A Demo

Requirements:

* Python 3.8.10 with Flask module 
* Docker [Install Ref: https://docs.docker.com/get-docker/

About the project:

It's simple application coded in Python with Flask as web server. We have functions.py file which contains the code which executes and 
returns all the data requested for the project.

The functions.py it's imported into server.py which then triggers the fuctions to return the data in json using jsonify from Flask.

Dockerfile:

The Dockerfile it's included on this project, using an Python 3.8.10 alphine version to ensure that the image uses the least possible elements to keep the disk and image size
as low as possible.

Inside the Dockefile we create a specific user to ensure that the application will run as non-root.

To push to your Dockerhub account you need the following commands:

- docker login
- docker build -t <name>:<your-tag> -f Dockerfile . 
- docker tag <name>:<your-tag> <your-repo>/<name>:<your-tag>
- docker push <your-repo>/<name>:<your-tag>

Kubernettes Deployment and Service

We included two .yml files that can be used with any Kubernettes active cluster. 

The deployment.yml describes how to deploy the application and links to the public DockerHub repository. You may need to change the <your-repo>/<name>:<your-tag> at spec: > containers: 
section to match your repository url/path.

The service.yml will create the service between the node and the container both running on port 3000.

To import the files you need the following commands on your master node:

- kubectl apply -f plan-a-demo-k8s-deployment.yml
- kubectl apply -f plan-a-demo-k8s-service.yml

You can then look at the pods to get the private IP:

ubuntu@master:~$ kubectl get pods -o wide
NAME                                     READY   STATUS    RESTARTS   AGE    IP           NODE      NOMINATED NODE   READINESS GATES
plan-a-demo-deployment-7fc5878cb-xktqm   1/1     Running   0          156m   100.44.0.1   worker1   <none>           <none>

and confirm the application is working by "curl" it:

ubuntu@master:~$ curl 100.44.0.1:3000
{"engine":"Flask 2.0.3","hostname":"plan-a-demo-deployment-7fc5878cb-xktqm (IP: 100.44.0.1)","timestamp:":"01-03-2022 14:57:47","visitor ip":"100.32.0.1"}