module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.12.0"

  name                 = "plan-a-demo"
  cidr                 = "10.101.0.0/16"
  azs                  = ["us-east-2a", "us-east-2b"]
  private_subnets      = ["10.101.0.0/19", "10.101.32.0/19"]
  public_subnets       = ["10.101.128.0/19", "10.101.160.0/19"]
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  public_subnet_tags = {
    "Name" = "plan-a-demo"
    "Type" = "Private Subnet"
  }

  private_subnet_tags = {
    "Name" = "plan-a-demo"
    "Type" = "Public Subnet"
  }
}
