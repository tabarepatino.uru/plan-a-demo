# Plan A Demo

Requirements:  

* Access to AWS via Access Keys  
* AWS CLI [Installation Ref. https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html]  
* Terraform [Installation Ref. https://learn.hashicorp.com/tutorials/terraform/install-cli]
* kubectl 1.21 [Installation Ref. https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html]

Deploy Instructions:  

* Configure AWS Profile with your Access Keys:
  - aws configure --profile plan-a-demo 
  > Default Region: us-east-2

* Execute the following commands to deploy:
  - terraform init
  - terraform plan
  - terraform apply

* Once Terraform completes, you can access using the following commands:
  - aws eks --region us-east-2 update-kubeconfig --name plan-a-demo --profile plan-a-demo

That will configure kubeconfig for you. You can now list the nodes with:
  - kubectl get nodes