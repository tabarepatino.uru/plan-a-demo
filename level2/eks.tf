module "eks" {
  source       = "terraform-aws-modules/eks/aws"
  version      = "17.24.0"
  cluster_name = "plan-a-demo"
  cluster_version = "1.21"
  vpc_id       = module.vpc.vpc_id
  subnets      = module.vpc.private_subnets

  node_groups = {
    eks_nodes = {
      desired_capacity = 2
      max_capacity     = 2
      min_capaicty     = 2

      instance_type = "t3a.large"
    }
  }

  tags = {
    Name = "plan-a-demo k8s Cluster"
  }

  write_kubeconfig = true
  kubeconfig_output_path = "/.kube/"
  manage_aws_auth = false
}